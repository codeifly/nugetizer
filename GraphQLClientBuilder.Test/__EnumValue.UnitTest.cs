using System;
using Xunit;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace CODEiFLY.GraphQL.Introspection.Test
{
    public class __EnumValueTest
    {
        [Fact(DisplayName = "Serialize & desirailize Name property")]
        public void SerializeName()
        {
            var t = JsonConvert.DeserializeObject<__EnumValue>($"{{{_nameJson}}}");
            Assert.Equal(_stringData, t.Name);
            Assert.True(JsonConvert.SerializeObject(t).IndexOf(_nameJson) > -1);
        }

        [Fact(DisplayName = "Serialize & desirailize Description property")]
        public void SerializeDescription()
        {
            var t = JsonConvert.DeserializeObject<__EnumValue>($"{{{_descriptionJson}}}");
            Assert.Equal(_stringData, t.Description);
            Assert.True(JsonConvert.SerializeObject(t).IndexOf(_descriptionJson) > -1);
        }

        [Fact(DisplayName = "Serialize & desirailize IsDeprecated property")]
        public void SerializeIsDeprecated()
        {
            var t = JsonConvert.DeserializeObject<__EnumValue>($"{{{_isDeprecatedJson}}}");
            Assert.Equal(_boolData, t.IsDeprecated);
            Assert.True(JsonConvert.SerializeObject(t).IndexOf(_isDeprecatedJson) > -1);
        }

        [Fact(DisplayName = "Serialize & desirailize DeprecationReason property")]
        public void SerializeDeprecationReason()
        {
            var t = JsonConvert.DeserializeObject<__EnumValue>($"{{{_deprecationReasonJson}}}");
            Assert.Equal(_stringData, t.DeprecationReason);
            Assert.True(JsonConvert.SerializeObject(t).IndexOf(_deprecationReasonJson) > -1);
        }

        private string _nameJson => $"\"name\":\"{_stringData}\"";
        private string _descriptionJson => $"\"description\":\"{_stringData}\"";
        private string _isDeprecatedJson => $"\"isDeprecated\":{JsonConvert.ToString(_boolData)}";
        private string _deprecationReasonJson => $"\"deprecationReason\":\"{_stringData}\"";
        private string _stringData = "testdata";
        private bool _boolData = true;
    }
}
