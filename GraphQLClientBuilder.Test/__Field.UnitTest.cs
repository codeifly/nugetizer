using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace CODEiFLY.GraphQL.Introspection.Test
{
    public class __FieldTest
    {
        [Fact(DisplayName = "Serialize & desirailize Name property")]
        public void SerializeName()
        {
            var t = JsonConvert.DeserializeObject<__Field>($"{{{_nameJson}}}");
            Assert.Equal(_stringData, t.Name);
            Assert.True(JsonConvert.SerializeObject(t).IndexOf(_nameJson) > -1);
        }
        private string _nameJson => $"\"name\":\"{_stringData}\"";

        [Fact(DisplayName = "Serialize & desirailize Description property")]
        public void SerializeDescription()
        {
            var t = JsonConvert.DeserializeObject<__Field>($"{{{_descriptionJson}}}");
            Assert.Equal(_stringData, t.Description);
            Assert.True(JsonConvert.SerializeObject(t).IndexOf(_descriptionJson) > -1);
        }
        private string _descriptionJson => $"\"description\":\"{_stringData}\"";

        [Fact(DisplayName = "Serialize & desirailize Args property")]
        public void SerializeArgs()
        {
            var t = JsonConvert.DeserializeObject<__Field>($"{{{_argsJSON}}}");
            Assert.Equal(_stringData, t.Args.FirstOrDefault().Name);
            Assert.True(JsonConvert.SerializeObject(t).IndexOf(_argsJSON) > -1);
        }
        private string _argsJSON => $"\"args\":{JsonConvert.SerializeObject(ArgsData())}";
        private List<__InputValue> ArgsData()
        {
            var args = new List<__InputValue>();
            for (int i = 0; i < 3; i++)
            {
                args.Add(_anInputValue);
            }
            return args;
        }
        private __InputValue _anInputValue =>
            new __InputValue()
            {
                Name = _stringData,
            };


        [Fact(DisplayName = "Serialize & desirailize Type property")]
        public void SerializeType()
        {
            var t = JsonConvert.DeserializeObject<__Field>($"{{{_typeJSON}}}");
            Assert.Equal(_stringData, t.Type.Name);
            Assert.True(JsonConvert.SerializeObject(t).IndexOf(_typeJSON) > -1);
        }
        private string _typeJSON => $"\"type\":{JsonConvert.SerializeObject(_typeData)}";
        private __Type _typeData => new __Type()
        {
            Name = _stringData,
        };

        [Fact(DisplayName = "Serialize & desirailize IsDeprecated property")]
        public void SerializeIsDeprecated()
        {
            var t = JsonConvert.DeserializeObject<__Field>($"{{{_isDeprecatedJson}}}");
            Assert.Equal(_boolData, t.IsDeprecated);
            Assert.True(JsonConvert.SerializeObject(t).IndexOf(_isDeprecatedJson) > -1);
        }
        private string _isDeprecatedJson => $"\"isDeprecated\":{JsonConvert.ToString(_boolData)}";

        [Fact(DisplayName = "Serialize & desirailize DeprecationReason property")]
        public void SerializeDeprecationReason()
        {
            var t = JsonConvert.DeserializeObject<__Field>($"{{{_deprecationReasonJson}}}");
            Assert.Equal(_stringData, t.DeprecationReason);
            Assert.True(JsonConvert.SerializeObject(t).IndexOf(_deprecationReasonJson) > -1);
        }
        private string _deprecationReasonJson => $"\"deprecationReason\":\"{_stringData}\"";

        private string _stringData = "testdata";
        private bool _boolData = true;
    }
}
