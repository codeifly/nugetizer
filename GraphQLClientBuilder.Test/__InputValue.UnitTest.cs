using System;
using Xunit;
using Newtonsoft.Json;

namespace CODEiFLY.GraphQL.Introspection.Test
{
    public class __InputValueTest
    {
        [Fact(DisplayName = "Serialize & desirailize Name property")]
        public void SerializeName()
        {
            var t = JsonConvert.DeserializeObject<__InputValue>($"{{{_nameJson}}}");
            Assert.Equal(_stringData, t.Name);
            Assert.True(JsonConvert.SerializeObject(t).IndexOf(_nameJson) > -1);
        }

        [Fact(DisplayName = "Serialize & desirailize Description property")]
        public void SerializeDescription()
        {
            var t = JsonConvert.DeserializeObject<__InputValue>($"{{{_descriptionJson}}}");
            Assert.Equal(_stringData, t.Description);
            Assert.True(JsonConvert.SerializeObject(t).IndexOf(_descriptionJson) > -1);
        }

        [Fact(DisplayName = "Serialize & desirailize Type property")]
        public void SerializeType()
        {
            var t = JsonConvert.DeserializeObject<__InputValue>($"{{{_typeJSON}}}");
            Assert.Equal(_stringData, t.Type.Name);
            Assert.True(JsonConvert.SerializeObject(t).IndexOf(_typeJSON) > -1);
        }

        [Fact(DisplayName = "Serialize & desirailize DeafultValue property")]
        public void SerializeDefaultValue()
        {
            var t = JsonConvert.DeserializeObject<__InputValue>($"{{{_defaultValueJson}}}");
            Assert.Equal(_stringData, t.DefaultValue);
            Assert.True(JsonConvert.SerializeObject(t).IndexOf(_defaultValueJson) > -1);
        }

        private string _nameJson => $"\"name\":\"{_stringData}\"";
        private string _descriptionJson => $"\"description\":\"{_stringData}\"";
        private string _typeJSON => $"\"type\":{JsonConvert.SerializeObject(_typeData)}";
        private __Type _typeData => new __Type()
        {
            Name = _stringData,
        };
        private string _defaultValueJson => $"\"defaultValue\":\"{_stringData}\"";
        private string _stringData = "testdata";
        private bool _boolData = true;
    }
}
