using System;
using Xunit;
using Newtonsoft.Json;
using System.Collections.Generic;
using Newtonsoft.Json.Converters;
using System.Linq;

namespace CODEiFLY.GraphQL.Introspection.Test
{
    public class __TypeTest
    {
        [Fact(DisplayName = "Serialize & desirailize Kind property")]
        public void SerializeKind()
        {
            var t = JsonConvert.DeserializeObject<__Type>($"{{{_kindJson}}}");
            Assert.Equal(__TypeKind.InputObject, t.Kind);
            Assert.True(JsonConvert.SerializeObject(t).IndexOf(_kindJson) > -1);
        }
        private string _kindJson => $"\"kind\":\"INPUT_OBJECT\"";

        [Fact(DisplayName = "Serialize & desirailize Name property")]
        public void SerializeName()
        {
            var t = JsonConvert.DeserializeObject<__Type>($"{{{_nameJson}}}");
            Assert.Equal(_stringData, t.Name);
            Assert.True(JsonConvert.SerializeObject(t).IndexOf(_nameJson) > -1);
        }
        private string _nameJson => $"\"name\":\"{_stringData}\"";

        [Fact(DisplayName = "Serialize & desirailize Description property")]
        public void SerializeDescription()
        {
            var t = JsonConvert.DeserializeObject<__Type>($"{{{_descriptionJson}}}");
            Assert.Equal(_stringData, t.Description);
            Assert.True(JsonConvert.SerializeObject(t).IndexOf(_descriptionJson) > -1);
        }
        private string _descriptionJson => $"\"description\":\"{_stringData}\"";

        [Fact(DisplayName = "Serialize & desirailize Fields property")]
        public void SerializeFields()
        {
            var t = JsonConvert.DeserializeObject<__Type>($"{{{_fieldsJSON}}}");
            Assert.Equal(_stringData, t.AllFields.FirstOrDefault().Name);
            Assert.True(JsonConvert.SerializeObject(t).IndexOf(_fieldsJSON) > -1);
        }
        private string _fieldsJSON => $"\"fields\":{JsonConvert.SerializeObject(FieldsData())}";
        private List<__Field> FieldsData()
        {
            var fields = new List<__Field>();
            for (int i = 0; i < 3; i++)
            {
                fields.Add(_aFieldValue);
            }
            return fields;
        }
        private __Field _aFieldValue =>
            new __Field()
            {
                Name = _stringData,                
            };

        [Fact(DisplayName = "Work Fields with arguments")]
        public void FieldsWork()
        {
            var t = new __Type()
            {
                AllFields = new List<__Field>()
                {
                    new __Field() { Name = _stringData, IsDeprecated = true },
                    new __Field() { Name = _stringData, IsDeprecated = false },
                }
            };
            Assert.Single(t.Fields());
            Assert.Equal(2, t.Fields(true).Count);
        }

        [Fact(DisplayName = "Serialize & desirailize Interfaces property")]
        public void SerializeInterfaces()
        {
            var t = JsonConvert.DeserializeObject<__Type>($"{{{_interfacesJSON}}}");
            Assert.Equal(_stringData, t.Interfaces.FirstOrDefault().Name);
            Assert.True(JsonConvert.SerializeObject(t).IndexOf(_interfacesJSON) > -1);
        }
        private string _interfacesJSON => $"\"interfaces\":{JsonConvert.SerializeObject(InterfacesData())}";
        private List<__Type> InterfacesData()
        {
            var fields = new List<__Type>();
            for (int i = 0; i < 3; i++)
            {
                fields.Add(_anInterfaceValue);
            }
            return fields;
        }
        private __Type _anInterfaceValue =>
            new __Type()
            {
                Name = _stringData,
            };

        [Fact(DisplayName = "Serialize & desirailize PosibleTypes property")]
        public void SerializePosibleTypes()
        {
            var t = JsonConvert.DeserializeObject<__Type>($"{{{_posibleTypesJSON}}}");
            Assert.Equal(_stringData, t.PosibleTypes.FirstOrDefault().Name);
            string json = JsonConvert.SerializeObject(t);
            int pos = json.IndexOf(_interfacesJSON);
            Assert.True(JsonConvert.SerializeObject(t).IndexOf(_posibleTypesJSON) > -1);
        }
        private string _posibleTypesJSON => $"\"posibleTypes\":{JsonConvert.SerializeObject(PosibleTypesData())}";
        private List<__Type> PosibleTypesData()
        {
            var fields = new List<__Type>();
            for (int i = 0; i < 3; i++)
            {
                fields.Add(_anPosibleTypesValue);
            }
            return fields;
        }
        private __Type _anPosibleTypesValue =>
            new __Type()
            {
                Name = _stringData,
            };

        [Fact(DisplayName = "Serialize & desirailize EnumValues property")]
        public void SerializeEnumValues()
        {
            var t = JsonConvert.DeserializeObject<__Type>($"{{{_enumValuesJSON}}}");
            Assert.Equal(_stringData, t.AllEnumValues.FirstOrDefault().Name);
            Assert.True(JsonConvert.SerializeObject(t).IndexOf(_enumValuesJSON) > -1);
        }
        private string _enumValuesJSON => $"\"enumValues\":{JsonConvert.SerializeObject(EnumValuesData())}";
        private List<__EnumValue> EnumValuesData()
        {
            var fields = new List<__EnumValue>();
            for (int i = 0; i < 3; i++)
            {
                fields.Add(_anEnumValueValue);
            }
            return fields;
        }
        private __EnumValue _anEnumValueValue =>
            new __EnumValue()
            {
                Name = _stringData,
            };

        [Fact(DisplayName = "Work EnumValues with arguments")]
        public void EnumValuesWork()
        {
            var t = new __Type()
            {
                AllEnumValues = new List<__EnumValue>()
                {
                    new __EnumValue() { Name = _stringData, IsDeprecated = true },
                    new __EnumValue() { Name = _stringData, IsDeprecated = false },
                }
            };
            Assert.Single(t.EnumValues());
            Assert.Equal(2, t.EnumValues(true).Count);
        }

        [Fact(DisplayName = "Serialize & desirailize InputFields property")]
        public void SerializeInputFields()
        {
            var t = JsonConvert.DeserializeObject<__Type>($"{{{_inputFieldsJSON}}}");
            Assert.Equal(_stringData, t.InputFields.FirstOrDefault().Name);
            Assert.True(JsonConvert.SerializeObject(t).IndexOf(_inputFieldsJSON) > -1);
        }
        private string _inputFieldsJSON => $"\"inputFields\":{JsonConvert.SerializeObject(InputFieldsData())}";
        private List<__InputValue> InputFieldsData()
        {
            var inputFields = new List<__InputValue>();
            for (int i = 0; i < 3; i++)
            {
                inputFields.Add(_anInputFieldsValue);
            }
            return inputFields;
        }
        private __InputValue _anInputFieldsValue =>
            new __InputValue()
            {
                Name = _stringData,
            };

        [Fact(DisplayName = "Serialize & desirailize OfType property")]
        public void SerializeOfType()
        {
            var t = JsonConvert.DeserializeObject<__Type>($"{{{_ofTypeJSON}}}");
            Assert.Equal(_stringData, t.OfType.Name);
            Assert.True(JsonConvert.SerializeObject(t).IndexOf(_ofTypeJSON) > -1);
        }
        private string _ofTypeJSON => $"\"ofType\":{JsonConvert.SerializeObject(_ofTypeData)}";
        private __Type _ofTypeData => new __Type()
        {
            Name = _stringData,
        };

        private string _stringData = "testdata";
    }
}
