using System;
using Xunit;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace CODEiFLY.GraphQL.Introspection.Test
{
    public class __TypeKindTest
    {
        [Fact(DisplayName = "Check namming conversion")]
        public void CheckNammingConversion()
        {
            foreach (var item in namming)
            {
                Assert.Equal(item.StringValue, JsonConvert.SerializeObject(item.EnumValue));
            }            
        }

        private List<Definition> namming = new List<Definition>()
        {
            new Definition()
            {
                EnumValue = __TypeKind.Enum,
                StringValue = "\"ENUM\"",
            },
            new Definition()
            {
                EnumValue = __TypeKind.InputObject,
                StringValue = "\"INPUT_OBJECT\"",
            },
            new Definition()
            {
                EnumValue = __TypeKind.Interface,
                StringValue = "\"INTERFACE\"",
            },
            new Definition()
            {
                EnumValue = __TypeKind.List,
                StringValue = "\"LIST\"",
            },
            new Definition()
            {
                EnumValue = __TypeKind.NonNull,
                StringValue = "\"NON_NULL\"",
            },
            new Definition()
            {
                EnumValue = __TypeKind.Object,
                StringValue = "\"OBJECT\"",
            },
            new Definition()
            {
                EnumValue = __TypeKind.Scalar,
                StringValue = "\"SCALAR\"",
            },
            new Definition()
            {
                EnumValue = __TypeKind.Union,
                StringValue = "\"UNION\"",
            }
        };

        private class Definition
        {
            public string StringValue { get; set; }

            public __TypeKind EnumValue { get; set; }
        }
    }
}
