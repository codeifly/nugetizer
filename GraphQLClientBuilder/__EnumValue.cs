﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CODEiFLY.GraphQL.Introspection
{
    /// <summary>
    /// The __EnumValue type represents one of possible values of an enum.
    /// </summary>
    public class __EnumValue
    {
        /// <summary>
        /// Enum name
        /// name: String!
        /// </summary>
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        /// <summary>
        /// Description for this enum value
        /// description: String
        /// </summary>
        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }

        /// <summary>
        /// Indicates if this enum value is depcrecated
        /// true if this field should no longer be used, otherwise false
        /// isDeprecated: Boolean!
        /// </summary>
        [JsonProperty(PropertyName = "isDeprecated")]
        public bool IsDeprecated { get; set; }

        /// <summary>
        /// Optionally provides a reason why this enum value is deprecated
        /// deprecationReason: String
        /// </summary>
        [JsonProperty(PropertyName = "deprecationReason")]
        public string DeprecationReason { get; set; }
    }
}
