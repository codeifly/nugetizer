﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CODEiFLY.GraphQL.Introspection
{
    /// <summary>
    /// The __Field type represents each field in an Object or Interface type.
    /// </summary>
    public class __Field
    {
        /// <summary>
        /// Field name
        /// name: String!
        /// </summary>
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        /// <summary>
        /// Description for this Field
        /// description: String
        /// </summary>
        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }

        /// <summary>
        /// List of __InputValue representing the arguments this field accepts.
        /// args: [__InputValue!]!
        /// </summary>
        [JsonProperty(PropertyName = "args")]
        public List<__InputValue> Args { get; set; }

        /// <summary>
        /// __Type that represents the type of value returned by this field.
        /// type: __Type!
        /// </summary>
        [JsonProperty(PropertyName = "type")]
        public __Type Type { get; set; }

        /// <summary>
        /// Indicates if this enum value is depcrecated
        /// true if this field should no longer be used, otherwise false
        /// isDeprecated: Boolean!
        /// </summary>
        [JsonProperty(PropertyName = "isDeprecated")]
        public bool IsDeprecated { get; set; }

        /// <summary>
        /// Optionally provides a reason why this field is deprecated
        /// deprecationReason: String
        /// </summary>
        [JsonProperty(PropertyName = "deprecationReason")]
        public string DeprecationReason { get; set; }
    }
}
