﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CODEiFLY.GraphQL.Introspection
{
    /// <summary>
    /// The __InputValue type represents field and directive arguments as well as the inputFields of an input object.
    /// </summary>
    public class __InputValue
    {
        /// <summary>
        /// InputValue name
        /// name: String!
        /// </summary>
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        /// <summary>
        /// Description for this InputValue
        /// description: String
        /// </summary>
        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }

        /// <summary>
        /// That represents the type this input value expects
        /// type: __Type!
        /// </summary>
        [JsonProperty(PropertyName = "type")]
        public __Type Type { get; set; }

        /// <summary>
        /// May return a String encoding (using the GraphQL language) of the default value used by this input value in the condition a value is not provided at runtime. 
        /// If this input value has no default value, returns null.
        /// defaultValue: String
        /// </summary>
        [JsonProperty(PropertyName = "defaultValue")]
        public string DefaultValue { get; set; }
    }
}
