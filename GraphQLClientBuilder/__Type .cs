﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace CODEiFLY.GraphQL.Introspection
{
    /// <summary>
    /// __Type is at the core of the type introspection system. 
    /// It represents scalars, interfaces, object types, unions, enums in the system. 
    /// __Type also represents type modifiers, which are used to modify a type that it refers to(ofType: __Type). 
    /// This is how we represent lists, non‐nullable types, and the combinations thereof.
    /// </summary>
    public class __Type
    {
        /// <summary>
        /// Kind of this Type
        /// kind: __TypeKind!
        /// </summary>
        [JsonProperty(PropertyName = "kind")]
        public __TypeKind Kind { get; set; }

        /// <summary>
        /// Name of this type
        /// name: String
        /// </summary>
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        /// <summary>
        /// Description of this type
        /// description: String
        /// </summary>
        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }

        /// <summary>
        /// List of Fields
        /// OBJECT and INTERFACE only
        /// fields(includeDeprecated: Boolean = false): [__Field!]
        /// </summary>
        [JsonProperty(PropertyName = "fields")]
        public List<__Field> AllFields { get; set; }

        /// <summary>
        /// Smart Fields getter
        /// Add Argument support for Fields property
        /// fields(includeDeprecated: Boolean = false): [__Field!]
        /// </summary>
        /// <param name="includeDeprecated">If true, deprecated fields are also returned.</param>
        /// <returns>List of Fields</returns>
        public List<__Field> Fields(bool includeDeprecated = false)
        {
            if (includeDeprecated)
            {
                return AllFields;
            }
            else
            {
                return AllFields != null ? AllFields.Where(f => !f.IsDeprecated).ToList() : null;
            }
        }

        /// <summary>
        /// List of interfaces that this object implements
        /// OBJECT only
        /// interfaces: [__Type!]
        /// </summary>
        [JsonProperty(PropertyName = "interfaces")]
        public List<__Type> Interfaces { get; set; }

        /// <summary>
        /// List of types that implements this interface or union
        /// INTERFACE and UNION only
        /// possibleTypes: [__Type!]
        /// </summary>
        [JsonProperty(PropertyName = "posibleTypes")]
        public List<__Type> PosibleTypes { get; set; }

        /// <summary>
        /// List of posible enum values
        /// ENUM only
        /// enumValues(includeDeprecated: Boolean = false): [__EnumValue!]
        /// </summary>
        [JsonProperty(PropertyName = "enumValues")]
        public List<__EnumValue> AllEnumValues { get; set; }

        /// <summary>
        /// Smart EnumValues getter
        /// Add Argument support for EnumValues property
        /// enumValues(includeDeprecated: Boolean = false): [__EnumValue!]
        /// </summary>
        /// <param name="includeDeprecated">If true, deprecated EnumValues are also returned.</param>
        /// <returns>List of EnumValues</returns>
        public List<__EnumValue> EnumValues(bool includeDeprecated = false)
        {
            if (includeDeprecated)
            {
                return AllEnumValues;
            }
            else
            {
                return AllEnumValues != null ? AllEnumValues.Where(f => !f.IsDeprecated).ToList() : null;
            }
        }

        /// <summary> 
        /// INPUT_OBJECT only
        /// inputFields: [__InputValue!]
        /// </summary>
        [JsonProperty(PropertyName = "inputFields")]
        public List<__InputValue> InputFields { get; set; }

        /// <summary>
        /// Define type of list items, if type is LIST or define concrate type is type is NOT_NULL type 
        /// NON_NULL and LIST only
        /// ofType: __Type
        /// </summary>
        [JsonProperty(PropertyName = "ofType")]
        public __Type OfType { get; set; }
    }
}
