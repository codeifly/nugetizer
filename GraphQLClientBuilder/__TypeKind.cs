﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Runtime.Serialization;

namespace CODEiFLY.GraphQL.Introspection
{
    /// <summary>
    /// There are several different kinds of type. In each kind, different fields are actually valid. 
    /// These kinds are listed in the __TypeKind enumeration.
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum __TypeKind
    {
        /// <summary>
        /// Represents scalar types such as Int, String, and Boolean. 
        /// Scalars cannot have fields.
        /// </summary>
        [EnumMember(Value = "SCALAR")]
        Scalar,
        /// <summary>
        /// Object types represent concrete instantiations of sets of fields. 
        /// The introspection types (e.g. __Type,  __Field, etc) are examples of objects.
        /// </summary>
        [EnumMember(Value = "OBJECT")]
        Object,
        /// <summary>
        /// Unions are an abstract type where no common fields are declared. 
        /// The possible types of a union are explicitly listed out in possibleTypes. 
        /// Types can be made parts of unions without modification of that type.
        /// </summary>
        [EnumMember(Value = "UNION")]
        Union,
        /// <summary>
        /// Interfaces are an abstract type where there are common fields declared. 
        /// Any type that implements an interface must define all the fields with names and types exactly matching. 
        /// The implementations of this interface are explicitly listed out in possibleTypes.
        /// </summary>
        [EnumMember(Value = "INTERFACE")]
        Interface,
        /// <summary>
        /// Enums are special scalars that can only have a defined set of values.
        /// </summary>
        [EnumMember(Value = "ENUM")]
        Enum,
        /// <summary>
        /// Input objects are composite types used as inputs into queries defined as a list of named input values.
        /// </summary>        
        [EnumMember(Value = "INPUT_OBJECT")]
        InputObject,
        /// <summary>
        /// Lists represent sequences of values in GraphQL. 
        /// A List type is a type modifier: it wraps another type instance in the ofType field, which defines the type of each item in the list.
        /// </summary>
        [EnumMember(Value = "LIST")]
        List,
        /// <summary>
        /// GraphQL types are nullable. The value null is a valid response for field type.
        /// A Non‐null type is a type modifier: it wraps another type instance in the ofType field. 
        /// Non‐null types do not allow null as a response, and indicate required inputs for arguments and input object fields.
        /// </summary>
        [EnumMember(Value = "NON_NULL")]
        NonNull,
    }
}
